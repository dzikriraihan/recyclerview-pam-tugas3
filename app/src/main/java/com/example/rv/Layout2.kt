package com.example.rv

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class Layout2 : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_layout2)

        val bundle : Bundle? = intent.extras

        val outNim: TextView = findViewById(R.id.nim)
        val outNama: TextView = findViewById(R.id.nama)

        outNim.text = bundle?.getString("nim")
        outNama.text = bundle?.getString("nama")


    }
}