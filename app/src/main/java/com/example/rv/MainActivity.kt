package com.example.rv


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class MainActivity : AppCompatActivity() {
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rv1:RecyclerView = findViewById(R.id.rv1)
        val TAG: String

        val data: ArrayList<Mahasiswa> = getData()
        val adapter = MahasiswaAdapter(this,data)
        rv1.adapter = adapter
        rv1.layoutManager = LinearLayoutManager(this)
        val submit: Button = findViewById(R.id.bt1)
        val etNama: EditText = findViewById(R.id.etNama)
        val etNim: EditText = findViewById(R.id.etNim)



        submit.setOnClickListener() {
            val nim = etNim.text.toString()
            val nama = etNama.text.toString()
            if(nim.isNotEmpty() && nama.isNotEmpty()) {
                val mhs = Mahasiswa()
                mhs.nim = nim
                mhs.nama = nama
                data.add(mhs)
                adapter.notifyDataSetChanged()

                etNim.setText("")
                etNama.setText("")
            } else {
                Toast.makeText(this@MainActivity, "Nama dan NIM harus diisi", Toast.LENGTH_SHORT)
                    .show()
            }

        }
    }


    fun getData():ArrayList<Mahasiswa> {
        val data = ArrayList<Mahasiswa>()
        val nim: List<String> = resources.getStringArray(R.array.nim).toList()
        val nama: List<String> = resources.getStringArray(R.array.nama).toList()
        for (i in nim.indices){
            val mhs = Mahasiswa()
            mhs.nim = nim[i]
            mhs.nama= nama[i]
            data.add(mhs)
        }
        return data
    }





}